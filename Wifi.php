<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;


class Wifi extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'wifi';
    protected $collection = 'wifi';
    protected $dates = ['created_at'];
    protected $dateFormat = 'Y-m-d';

}
