<?php

use App\Http\Controllers\WifiController;
use Illuminate\Support\Facades\Route;

Route::get('/case1_result', [WifiController::class, 'case1_result']);
Route::get('/case2_result', [WifiController::class, 'case2_result']);
/*
 * REVİZE ALANI
 */
Route::get('/case1_result_revize1', [WifiController::class, 'case1_result_revize1']);
Route::get('/case2_result_revize1', [WifiController::class, 'case2_result_revize1']);
Route::get('/case2_result_revize2', [WifiController::class, 'case2_result_revize2']);
