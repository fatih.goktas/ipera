<?php
// todo: kod optimizasyonu, son kullanıcı odaklı ve performans

namespace App\Http\Controllers;

use App\Models\Wifi;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class WifiController extends Controller
{
    public function __construct()
    {
        set_time_limit(-1);
        DB::enableQueryLog();
    }

    public function debug()
    {
        return DB::getQueryLog();
    }

    public function case1_cache()
    {
        // cache varsa silinir.
        Cache::forget('case1');
        // Yeniden cache oluşturulur
        Cache::remember('case1', 86400, function () {
            $data = Wifi::project(['dayOfYear' => ['$dayOfYear' => ['date' => '$created_at']], 'floor_id' => '$floor_id', 'mac' => '$mac'])
                ->groupBy('mac', 'floor_id', 'dayOfYear')
                ->get();
            // Cache mac adres listesi olarak düzenlenir.
            foreach ($data as $datum) {
                $value[$datum->mac] = $datum->mac;
            }
            return $value;
        });
    }

    public function case1_result()
    {
        header('Content-disposition: attachment; filename=case1.txt');
        header('Content-type: text/plain');
        $mac_string = "";
        // cahce'deki veri çekilir
        $data = Cache::get('case1');
        if (!$data) {
            // eğer cache verisi yoksa yeniden oluşturulur ve çekilir.
            $this->case1_cache();
            $data = Cache::get('case1');
        }
        // çıktı düzenlenir.
        echo "Count : " . count($data) . "\n\n\n";
        foreach ($data as $mac) {
            $mac_string .= $mac . "\n";
        }
        echo $mac_string;
    }

    public function case2_result()
    {
        header('Content-disposition: attachment; filename=case2.txt');
        header('Content-type: text/plain');
        // Veritabanındaki veri created_at alanına göre ASC sıralanarak değişkene alınır.
        $data = Wifi::orderBy('created_at', 'asc')->options(["allowDiskUse" => true])->get();
        $new_data2 = [];
        $idx = 0;
        foreach ($data as $index => $datum) {
            // new_data dizisinde ilgili mac adresine ait bir kayıt var mı kontrol edilir.
            if (!isset($new_data[$datum->mac])) {
                // kayıt yoksa, ilk defa karşılaşılan mac adresi için kayıt oluşturulur.
                $new_data[$datum->mac]["floor1"] = $datum->floor_id; // T1 anında kişinin bulunduğu kat kaydedilir.
                $new_data[$datum->mac]["t1"] = $datum->created_at->format('Y-m-d H:i:s'); // T1 anının kendisidir.
                $new_data[$datum->mac]["back"] = 0; // kişinin katlar arası hareketi sonrasında T1 anındaki kata geri dönüp dönmediğinin kontrolünü belirtir.
            } else {
                // kayıt varsa, mevcut mac adresi için işlem yapılır
                if ($new_data[$datum->mac]["floor1"] != $datum->floor_id) {
                    // T1 anındaki kat bilgisi ile başka herhangi bir andaki kat bilgisi eşleşmiyorsa kişi hareket ediyor farz edilir.
                    $new_data[$datum->mac]["floor2"] = $datum->floor_id; // Yeni kat bilgisi kaydedilir.
                    $new_data[$datum->mac]["t2"] = $datum->created_at->format('Y-m-d H:i:s'); // T2 anı hareket tespit edildiği için güncellenir.
                }
                // Yeni bir hareket varsa
                if (isset($new_data[$datum->mac]["t2"]) && $new_data[$datum->mac]["floor1"] == $datum->floor_id) {
                    // T1 anındaki kat bilgisi ile harekete bağlı yeni kat bilgisi eşleşiyorsa zıplama eylemi olabileceği düşünülür.
                    $startTime = Carbon::parse($new_data[$datum->mac]["t2"]); // T2 anındaki tarih bilgisi fark hesaplaması için Carbon ile parse edilir.
                    $finishTime = Carbon::parse($datum->created_at->format('Y-m-d H:i:s')); // Yeni hareket anındaki tarih bilgisi fark hesaplaması için Carbon ile parse edilir.
                    $diff_seconds = $finishTime->diffInSeconds($startTime); // T2 ve yeni hareket arasındaki fark saniye cinsinden hesaplanır.
                    if ($diff_seconds <= 120) {
                        // Tarihler arasındaki fark 120 saniyeye eşit veya küçükse zıplama eylemi olduğu netleşir.
                        // Yeni bir dizi içerisinde zıplama eylemi gerçekleştiren kişiler için kayıt alınır.
                        // Kıyas yapılabilmesi için kişiye ait geriye dönük bilgiler ile son bilgiler not edilir.
                        $new_data2[$idx]['mac'] = $datum->mac;
                        $new_data2[$idx]['f1'] = $new_data[$datum->mac]["floor1"];
                        $new_data2[$idx]['f2'] = $new_data[$datum->mac]["floor2"];
                        $new_data2[$idx]['f3'] = $datum->floor_id;
                        $new_data2[$idx]['t1'] = $new_data[$datum->mac]["t1"];
                        $new_data2[$idx]['t2'] = $new_data[$datum->mac]["t2"];
                        $new_data2[$idx]['t3'] = $datum->created_at->format('Y-m-d H:i:s');
                        $new_data2[$idx]['diff'] = $diff_seconds;
                        $new_data2[$idx]["back"] = "true";
                        $idx++;
                    }
                }
            }
        }

        // Hazırlanan veri dışarı aktarılır.
        $string = "Count : " . count($new_data2) . "\n\n\nMAC : FLOOR 1 : FLOOR 2 : FLOOR 3 : T1 : T2 : T3 : DIFF : BACK\n\n";
        foreach ($new_data2 as $item) {
            $string .= $item['mac'] . " : " .
                $item['f1'] . " : " .
                $item['f2'] . " : " .
                $item['f3'] . " : " .
                $item['t1'] . " : " .
                $item['t2'] . " : " .
                $item['t3'] . " : " .
                $item['diff'] . " : " .
                $item['back'] .
                "\n";
        }
        echo $string;
    }

    /*
     * REVİZE ALANI - CASE 1
     */

    public function case1_cache_revize1()
    {
        return Cache::remember('case1', 86400, function () {
            // değişken tanımlamaları
            $result = array();
            $html = "";
            // mongodb sorgusu
            $mac_list = Wifi::project(['dayOfYear' => ['$dayOfYear' => ['date' => '$created_at']], 'floor_id' => '$floor_id', 'mac' => '$mac'])
                ->groupBy('mac', 'floor_id', 'dayOfYear')
                ->get(['mac', 'floor_id', 'created_at']);
            // döngü çalıştırılır
            foreach ($mac_list as $element) {
                if ($result[$element->mac]['condition'] == 1) continue;
                // Döngüde daha önce oluşturulmuş dizi içerisindeki mac adresine ait koşul testi bu aşamada yapılır
                if (isset($result[$element->mac][$element->dayOfYear]) && count($result[$element->mac][$element->dayOfYear]) > 1) {
                    if ($result[$element->mac]['condition'] == 0) {
                        $html .= $element->mac . ",\n";
                        $result[$element->mac]['condition'] = 1;
                    }
                }
                // Eğer bir mac adresi için ilk defa döngü çalışıyorsa bir dizi oluşturulur.
                else {
                    $result[$element->mac][$element->dayOfYear][] = $element->floor_id;
                    $result[$element->mac]['condition'] = 0;
                }
            }
            return rtrim($html, ",");
        });
    }

    public function case1_result_revize1()
    {
        header('Content-disposition: attachment; filename=case1.txt');
        header('Content-type: text/plain');
        // Cache kaydı kontrol edilir. Varsa direkt olarak içeriği dışa aktarılır.
        $mac_list = Cache::get('case1');
        // Cache içeriği boş veya yok ise
        if (!$mac_list) {
            // Yeni cache ve içerik hazırlanır.
            $mac_list = $this->case1_cache_revize1();
        }
        echo $mac_list;
    }

    /*
     * REVİZE ALANI - CASE 2
     */

    public function case2_cache_revize1($list)
    {
        return Cache::remember('case2', 86400, function () use ($list) {
            $list = explode(',', $list);
            $html = "";
            foreach ($list as $index => $mac) {
                $new_data = [];
                $data = Wifi::where('mac', $mac)
                    ->orderBy('created_at', 'desc')
                    ->options(['scanAndOrder' => true, 'allowDiskUse' => true])
                    ->get(['mac','floor_id','created_at']);
                foreach ($data as $datum) {
                    if (isset($new_data[$datum->mac]['back']) && $new_data[$datum->mac]['back'] == 1) continue;
                    if (!isset($new_data[$datum->mac])) {
                        $new_data[$datum->mac]["floor1"] = $datum->floor_id;
                        $new_data[$datum->mac]["t1"] = $datum->created_at->format('Y-m-d H:i:s');
                        $new_data[$datum->mac]["back"] = 0;
                    } else {
                        if ($new_data[$datum->mac]["floor1"] != $datum->floor_id) {
                            $new_data[$datum->mac]["floor2"] = $datum->floor_id;
                            $new_data[$datum->mac]["t2"] = $datum->created_at->format('Y-m-d H:i:s');
                        }
                        if (isset($new_data[$datum->mac]["t2"]) && $new_data[$datum->mac]["floor1"] == $datum->floor_id) {
                            $startTime = Carbon::parse($new_data[$datum->mac]["t2"]);
                            $finishTime = Carbon::parse($datum->created_at->format('Y-m-d H:i:s'));
                            $diff_seconds = $finishTime->diffInSeconds($startTime);
                            if ($diff_seconds <= 120) {
                                $new_data[$datum->mac]['back'] = 1;
                                $new_data[$datum->mac]["floor3"] = $datum->floor_id;
                                $new_data[$datum->mac]["t3"] = $datum->created_at->format('Y-m-d H:i:s');
                                $html .= $datum->mac . ",\n";
                            }
                        }
                    }
                }
            }
            return rtrim($html, ',');
        });
    }

    public function case2_result_revize1()
    {
        header('Content-disposition: attachment; filename=case2.txt');
        header('Content-type: text/plain');
        $mac_list = Cache::get('case2');
        // Cache içeriği boş veya yok ise
        if (!$mac_list) {
            $mac_list = Cache::remember('case1', 86400, function () {
                $data = Wifi::options(['scanAndOrder' => true, 'allowDiskUse' => true])
                    ->orderBy('created_at', 'asc')
                    ->get(['mac', 'floor_id', 'created_at']);
                $html = "";
                foreach ($data as $index => $datum) {
                    if (isset($new_data[$datum->mac]['back']) && $new_data[$datum->mac]['back'] == 1) continue;
                    if (!isset($new_data[$datum->mac])) {
                        $new_data[$datum->mac]["floor1"] = $datum->floor_id;
                        $new_data[$datum->mac]["t1"] = $datum->created_at->format('Y-m-d H:i:s');
                        $new_data[$datum->mac]["back"] = 0;
                    } else {
                        if ($new_data[$datum->mac]["floor1"] != $datum->floor_id) {
                            $new_data[$datum->mac]["floor2"] = $datum->floor_id;
                            $new_data[$datum->mac]["t2"] = $datum->created_at->format('Y-m-d H:i:s');
                        }
                        if (isset($new_data[$datum->mac]["t2"]) && $new_data[$datum->mac]["floor1"] == $datum->floor_id) {
                            $startTime = Carbon::parse($new_data[$datum->mac]["t2"]);
                            $finishTime = Carbon::parse($datum->created_at->format('Y-m-d H:i:s'));
                            $diff_seconds = $finishTime->diffInSeconds($startTime);
                            if ($diff_seconds <= 120) {
                                $new_data[$datum->mac]['back'] = 1;
                                $new_data[$datum->mac]["floor3"] = $datum->floor_id;
                                $new_data[$datum->mac]["t3"] = $datum->created_at->format('Y-m-d H:i:s');
                                $html .= $datum->mac . "\n";
                            }
                        }
                    }
                }
                return $html;
            });
        }
        echo $mac_list;
    }

    public function case2_result_revize2()
    {
        header('Content-disposition: attachment; filename=case2.txt');
        header('Content-type: text/plain');
        $html = Cache::get('case2');
        if (!$html) {
            $mac_list = Cache::get('case1');
            if (!$mac_list) {
                $mac_list = $this->case1_cache_revize1();
            }
            $html = $this->case2_cache_revize1($mac_list);
        }
        echo rtrim($html, ',');
    }
}
